import mechanicalsoup
import re

URL = "https://www.cpubenchmark.net/cpu_list.php"

""" This module grabs looks up the Passmark cpu benchmark site and parses the
CPU table for Ryzen CPUs with their name and score
"""

AMD = {'1200': ['AMD Ryzen 3 PRO 1200', '6376'], '1300X': ['AMD Ryzen 3 1300X', '6887'], '2200G': ['AMD Ryzen 3 PRO 2200G', '6731'], '2200GE': ['AMD Ryzen 3 PRO 2200GE', '6223'], '2200U': ['AMD Ryzen 3 2200U', '3674'], '2300U': ['AMD Ryzen 3 PRO 2300U', '6149'], '2300X': ['AMD Ryzen 3 2300X', '7772'], '3100': ['AMD Ryzen 3 3100', '11745'], '3200G': ['AMD Ryzen 3 PRO 3200G', '7056'], '3200GE': ['AMD Ryzen 3 PRO 3200GE', '7113'], '3200U': ['AMD Ryzen 3 3200U', '4006'], '3250U': ['AMD Ryzen 3 3250U', '4232'], '3300U': ['AMD Ryzen 3 PRO 3300U', '6060'], '3300X': ['AMD Ryzen 3 3300X', '12765'], '4300G': ['AMD Ryzen 3 4300G', '11552'], '4300GE': ['AMD Ryzen 3 4300GE', '11878'], '4300U': ['AMD Ryzen 3 4300U', '7740'], '1300': ['AMD Ryzen 3 PRO 1300', '7717'], '2100GE': ['AMD Ryzen 3 PRO 2100GE', '4308'], '4200GE': ['AMD Ryzen 3 PRO 4200GE', '11006'], '4350G': ['AMD Ryzen 3 PRO 4350G', '11015'], '4350GE': ['AMD Ryzen 3 PRO 4350GE', '11443'], '4450U': ['AMD Ryzen 3 PRO 4450U', '10083'], '1400': ['AMD Ryzen 5 1400', '7828'], '1500X': ['AMD Ryzen 5 1500X', '9067'], '1600': ['AMD Ryzen 5 PRO 1600', '10401'], '1600X': ['AMD Ryzen 5 1600X', '13080'], '2400G': ['AMD Ryzen 5 PRO 2400G', '8457'], '2400GE': ['AMD Ryzen 5 PRO 2400GE', '7603'], '2500U': ['AMD Ryzen 5 PRO 2500U', '6586'], '2500X': ['AMD Ryzen 5 2500X', '9489'], '2600': ['AMD Ryzen 5 PRO 2600', '13662'], '2600H': ['AMD Ryzen 5 2600H', '7537'], '2600X': ['AMD Ryzen 5 2600X', '14076'], '3350G': ['AMD Ryzen 5 PRO 3350G', '9582'], '3350GE': ['AMD Ryzen 5 PRO 3350GE', '9643'], '3400G': ['AMD Ryzen 5 PRO 3400G', '9489'], '3400GE': ['AMD Ryzen 5 PRO 3400GE', '8183'], '3450U': ['AMD Ryzen 5 3450U', '7475'], '3500': ['AMD Ryzen 5 3500', '12804'], '3500U': ['AMD Ryzen 5 PRO 3500U', '6950'], '3500X': ['AMD Ryzen 5 3500X', '13427'], '3550H': ['AMD Ryzen 5 3550H', '8124'], '3550U': ['AMD Ryzen 5 3550U', '7681'], '3580U': ['AMD Ryzen 5 3580U', '7949'], '3600': ['AMD Ryzen 5 PRO 3600', '18037'], '3600X': ['AMD Ryzen 5 3600X', '18328'], '3600XT': [
    'AMD Ryzen 5 3600XT', '18884'], '4500U': ['AMD Ryzen 5 PRO 4500U', '12049'], '4600G': ['AMD Ryzen 5 4600G', '16957'], '4600GE': ['AMD Ryzen 5 4600GE', '16377'], '4600H': ['AMD Ryzen 5 4600H', '14865'], '4600HS': ['AMD Ryzen 5 4600HS', '14239'], '4600U': ['AMD Ryzen 5 4600U', '13835'], '5600X': ['AMD Ryzen 5 5600X', '22195'], '1500': ['AMD Ryzen 5 PRO 1500', '9434'], '4400G': ['AMD Ryzen 5 PRO 4400G', '16456'], '4400GE': ['AMD Ryzen 5 PRO 4400GE', '14795'], '4650G': ['AMD Ryzen 5 PRO 4650G', '16625'], '4650GE': ['AMD Ryzen 5 PRO 4650GE', '16571'], '4650U': ['AMD Ryzen 5 PRO 4650U', '12870'], '1700': ['AMD Ryzen 7 PRO 1700', '14593'], '1700X': ['AMD Ryzen 7 PRO 1700X', '15697'], '1800X': ['AMD Ryzen 7 1800X', '16272'], '2700': ['AMD Ryzen 7 PRO 2700', '15049'], '2700E': ['AMD Ryzen 7 2700E', '14657'], '2700U': ['AMD Ryzen 7 PRO 2700U', '7176'], '2700X': ['AMD Ryzen 7 PRO 2700X', '16846'], '2800H': ['AMD Ryzen 7 2800H', '7656'], '3700U': ['AMD Ryzen 7 PRO 3700U', '7390'], '3700X': ['AMD Ryzen 7 3700X', '22814'], '3750H': ['AMD Ryzen 7 3750H', '8410'], '3780U': ['AMD Ryzen 7 3780U', '7194'], '3800X': ['AMD Ryzen 7 3800X', '23367'], '3800XT': ['AMD Ryzen 7 3800XT', '23975'], '4700G': ['AMD Ryzen 7 PRO 4700G', '20848'], '4700GE': ['AMD Ryzen 7 4700GE', '20671'], '4700U': ['AMD Ryzen 7 4700U', '13801'], '4800H': ['AMD Ryzen 7 4800H', '19156'], '4800HS': ['AMD Ryzen 7 4800HS', '19033'], '4800U': ['AMD Ryzen 7 4800U', '17296'], '5800X': ['AMD Ryzen 7 5800X', '28724'], '3700': ['AMD Ryzen 7 PRO 3700', '21936'], '4750G': ['AMD Ryzen 7 PRO 4750G', '21059'], '4750GE': ['AMD Ryzen 7 PRO 4750GE', '20377'], '4750U': ['AMD Ryzen 7 PRO 4750U', '15673'], '3900': ['AMD Ryzen 9 PRO 3900', '31800'], '3900X': ['AMD Ryzen 9 3900X', '32859'], '3900XT': ['AMD Ryzen 9 3900XT', '33047'], '3950X': ['AMD Ryzen 9 3950X', '39297'], '4900H': ['AMD Ryzen 9 4900H', '19021'], '4900HS': ['AMD Ryzen 9 4900HS', '19868'], '5900X': ['AMD Ryzen 9 5900X', '39520'], '5950X': ['AMD Ryzen 9 5950X', '45968']}


""" Returns dictionary of all single socket CPUs on cpubenchmark.net """


def scores():
    cpus = {}
    browser = mechanicalsoup.StatefulBrowser()
    page = browser.open(URL)

    for entry in page.soup.find('table', attrs={"id": "cputable"}).find('tbody').find_all('tr'):
        rows = entry.find_all('td')
        cpus[rows[0].find('a').get_text()] = rows[1].get_text()
    return cpus


""" Returns dictionary of AMD Ryzen CPUs on cpubenchmark.net """


def ryzen():
    passmark = scores()
    ryzen = {}
    ryzen_re = re.compile(
        r"AMD Ryzen [3,5,7,9].*(\d{4}$|\d{4}\w$|\d{4}\w{2}$)")
    for cpu in passmark.keys():
        if ryzen_re.match(cpu):
            match = ryzen_re.match(cpu)
            ryzen.update(
                {match.groups()[0]: [cpu, passmark[cpu].replace(',', '')]})
    return ryzen


def main():
    amd_ryzen = ryzen()
    # for key in amd_ryzen.keys():
    #     print("CPU:\t{}\t{}".format(key, amd_ryzen[key]))
    print(amd_ryzen)


if __name__ == "__main__":
    main()
