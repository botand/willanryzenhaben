import mechanicalsoup
import passmark
import requests
import json
import re
import time

URL = "https://www.willhaben.at/iad/kaufen-und-verkaufen/marktplatz/pc-komponenten/cpus-prozessoren-5880?keyword=ryzen"
BASE = "https://www.willhaben.at"
TMS_RE = r".*var tmsJson = (?P<tmsJson>.*);$"
AMD_RE = r".*(?P<model>\d{4}\w{2}|\d{4}\w|\d{4})"
ACCESS_TOKEN = ""


def parse_offer(link: str):
    """change depending on how nice you are"""
    time.sleep(1)
    site = requests.get(link).text.split("\n")
    tms_re = re.compile(TMS_RE)
    data = []
    paylivery = False
    jsonbuff = "{}"
    for line in site:
        if "tmsJson" in line:
            data.append(line)
        if "PayLivery" in line:
            paylivery = True
    for line in data:
        print(f"matching line:\t{line}")
        if tms_re.match(line):
            print("yes")
            match = tms_re.match(line)
            jsonbuff = match.group("tmsJson")
            break
        else:
            print("no")
            continue
    ret = json.loads(jsonbuff)['tmsData']
    ret["paylivery"] = paylivery
    return ret


def get_offers(old: dict):
    ret = {}
    links = __get_ads(URL)
    for link in links:
        tmsJson = parse_offer(BASE+link)
        if tmsJson["ad_id"] in old.keys():
            break
        ret[tmsJson["ad_id"]] = tmsJson
    return ret


def __get_ads(url: str):
    ret = []
    browser = mechanicalsoup.StatefulBrowser()
    page = browser.open(url)
    for article in page.soup.find_all("article", attrs={"class": "search-result-entry"}):
        if article.find("section", attrs={"class", "fakeAdsLiElement"}):
            continue
        params = article.find("section", attrs={"class": "content-section"})
        ret.append(params.find("span", attrs={
                   "itemprop": "name"}).parent["href"])
    return ret


def dump_offers(offers: dict):
    with open('passmark.json', 'w') as f:
        f.truncate()
        json.dump(offers, f)


def read_offers():
    offers = {}
    print("reading JSON")
    with open('passmark.json') as f:
        offers = json.load(f)
    return offers


def __send_notification(title: str, body: dict, token: str):
    if len(body) == 0:
        return
    """ Adjust to your liking. Generally anything above 120 is a pretty nice deal"""
    if int(body['factor']) <= 100:
        return
    notification = "CPU: {}\nPRICE: {}\nFACTOR: {}\nSCORE: {}\nLINK: {}".format(
        body['name'], body['price'], body['factor'], body['score'], body['link'])
    data_send = {"type": "note", "title": title, "body": notification}
    resp = requests.post('https://api.pushbullet.com/v2/pushes', data=json.dumps(data_send),
                         headers={'Authorization': 'Bearer ' + token, 'Content-Type': 'application/json'})
    if resp.status_code != 200:
        raise Exception('Something wrong')
    else:
        print("Sent notification.")


def __check_passmark_score(entry: dict) -> dict:
    amd_re = re.compile(AMD_RE)
    if amd_re.match(entry['ad_title']):
        model = amd_re.match(entry['ad_title']).group('model')
        if model.upper not in passmark.AMD:
            return {}
        score = passmark.AMD[model.upper()][1]
        name = passmark.AMD[model.upper()][0]
        factor = float(score)/float(entry['exact_price'])
        price = float(entry['exact_price'])
        link = __get_url("https://www.willhaben.at/iad/finncode/result?finncode={}".format(
            entry['ad_id']))
        ret = {'model': model, 'score': score, 'name': name,
               'factor': factor, 'link': link, 'price': price}
        return ret
    return {}


def __get_url(url: str) -> str:
    r = requests.get(url)
    return str(r.url)


def notify(entries: dict):
    token = __get_apikey()
    for entry in entries:
        print("'{}'".format(entries[entry]['ad_title']))
        if not entries[entry]['paylivery']:
            continue
        body = __check_passmark_score(entries[entry])
        __send_notification("RYZEN ALERT", body, token)


def __get_apikey():
    token = ""
    with open('apikey', 'r') as apikey:
        token = apikey.read().split('\n')[0]
        print("'{}'".format(ACCESS_TOKEN))
    return token


def main():
    OLD = read_offers()
    print(len(OLD))
    NEW = get_offers(OLD)
    print(len(NEW))
    notify(NEW)
    updated = OLD | NEW
    dump_offers(updated)


if __name__ == "__main__":
    main()
